# Abusive IP Addresses for Loginizer

Most recent CSV: [2024-10-22](https://gitlab.com/gmcg/loginizer-blacklist/-/raw/main/2024/loginizer-blacklist-2024-10-22.csv)

## What is this?

An imperfect, _maintained by hand_ collection of IP ranges that have attempted to log into my WordPress site.

Any login attempt from an IP address that I do not recognise is looked up, and the entire range the IP address sits in is added to the blacklist.

## What can I do with this?

In "Loginizer Brute Force Settings" you can take the most recent file uploaded here and just import it. Simple as.

Though, it should be noted that some residential IP addresses may be included. Please make sure your Home IP (or an IP you typically log in from) is whitelisted before importing the blacklist.
